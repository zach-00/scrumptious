from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe, Ingredient
from recipes.forms import RecipeForm, IngredientForm
from django.contrib.auth.decorators import login_required

# Create your views here.
def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": recipe,
    }
    return render(request, "recipes/detail.html", context)


def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)


@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)



@login_required
def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        # We should use the form to validate the values
        # and save them to the database
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()
            # If all goes well, we can redirect the browswer
            # to another page and leave the function
            return redirect("recipe_list")
    else:
        # else, create an instance of the Django model form class
        form = RecipeForm()

    # put the form in the context to go into the HTML template
    context = {
        "form": form,
    }
    # render the HTML template with the form
    return render(request, "recipes/create.html", context)


def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()

    else:
        form = RecipeForm(instance=recipe)

    context = {
        "recipe_object": recipe, # the var recipe is literally just one of the recipe objects created from the model class
        "recipe_form": form, # form is the entire recipe form that we created in the forms file
    }

    return render(request, "recipes/edit.html", context)
